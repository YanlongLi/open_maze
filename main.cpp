#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

//find just the first solution. All solutions can be found but too long

ifstream fin("test.in");
ofstream fout("test.out");

void printResult(vector<vector<char> >& maze, int row, int col, const vector<pair<int,int> > &res){
    for (int i = 0; i < res.size(); ++i) {
        maze[res[i].first][res[i].second] = '.';
    }
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            fout << maze[i][j];
        }
        fout << endl;
    }
}

bool dfs(const vector<vector<char> >& maze, vector<vector<bool> >&visited, int startx, int starty, int destx, int desty, int row, int col, vector<pair<int, int> >&res) {
    if (startx < 0 || startx >= row || starty < 0 || starty >= col) {
        return false;
    }
    if (startx == destx && starty == desty) {
        return true;
    }
    if (visited[startx][starty] == true || maze[startx][starty] == '%') return false;

    visited[startx][starty] = true;
    res.push_back(make_pair(startx, starty));
    // cout << "x: " << p.first << " y: " << p.second << endl;
    bool find =
        dfs(maze, visited, startx + 1, starty, destx, desty, row, col, res) ||
        dfs(maze, visited, startx - 1, starty, destx, desty, row, col, res) ||
        dfs(maze, visited, startx, starty + 1, destx, desty, row, col, res) ||
        dfs(maze, visited, startx, starty - 1, destx, desty, row, col, res);
    // visited[startx][starty] = false;
    if(find) return true;
    res.pop_back();
    return false;
}
int main() {
    //read the matrix
    string buff;
    vector<vector<char> > maze(100, vector<char>(100));
    int col;
    int row = 0;
    while (!fin.eof()) {
        getline(fin, buff);
        col = buff.length();
        for (int i = 0; i < col; ++i) {
            maze[row][i] = buff[i];
        }
        row++;
    }
    vector<vector<bool> > visited(row, vector<bool>(col));
    //find the start
    int startx, starty;
    int destx, desty;
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            if (maze[i][j] == '.') {
                startx = i;
                starty = j;
            }
            if (maze[i][j] == 'P') {
                destx = i;
                desty = j;
            }
        }
    }
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) visited[i][j] = false;
    }
    maze[startx][starty] = ' ';

    // dfs
    vector<pair<int, int> >res;
    bool find = dfs(maze, visited, startx, starty, destx, desty, row, col, res);
    if(find){
        printResult(maze, row, col, res);
    } else {
        fout<<"Solution Not Found"<<endl;
    }
    return 0;
}

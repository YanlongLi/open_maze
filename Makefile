
main: main.cpp
	g++ main.cpp -o main

test.out: main test.in
	./main
	@cat test.out

clean:
	@rm -f a.out main test.out

.PHONY: clean
